// default package
// Generated Sep 10, 2014 2:41:39 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

/**
 * News generated by hbm2java
 */
public class News implements java.io.Serializable {

	private Integer id;
	private String title;
	private String content;
	private Date composeday;

	public News() {
	}

	public News(String title) {
		this.title = title;
	}

	public News(String title, String content, Date composeday) {
		this.title = title;
		this.content = content;
		this.composeday = composeday;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getComposeday() {
		return this.composeday;
	}

	public void setComposeday(Date composeday) {
		this.composeday = composeday;
	}

}
