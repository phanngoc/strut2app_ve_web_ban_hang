// default package
// Generated Sep 10, 2014 2:41:39 PM by Hibernate Tools 3.4.0.CR1

/**
 * Category generated by hbm2java
 */
public class Category implements java.io.Serializable {

	private Integer id;
	private String name;
	private Integer idType;

	public Category() {
	}

	public Category(String name, Integer idType) {
		this.name = name;
		this.idType = idType;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getIdType() {
		return this.idType;
	}

	public void setIdType(Integer idType) {
		this.idType = idType;
	}

}
