<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<script>
	$(document).ready(function(){
		//$("#news-list tr").append("<a=\"#\" class=\"readmore\">Read more</a>");
		  $('#pagination-demo').twbsPagination({
		        totalPages: '<s:property value="totalPages"/>',
		        visiblePages: 3,
		        startPage : <s:property value="indexpage"/>,
		        href: '?indexpage={{number}}',
		        onPageClick: function (event, page) {
		            //$('#page-content').text('Page ' + page);
		        }
		    });
	});
</script>

<div id="list-news" class="container">
 
  	 <s:iterator value="newshow">
  	    <div class="row">
  	      <div class="container">
  	      	<div class="row">
  	      		<h4><a href="detail.action?idnew=<s:property value="id"/>"><s:property value="title"/></a></h4>
  	      	</div>
  	      	<div class="row">
  	      		<p><s:date name="composeday" format="dd/MM/yyyy" /></p>
  	      	</div>
  	      	<div class="row">
  	      		<s:property value="content"/>
  	      	</div>
  	      	<div class="row">
  	      		<a href="detail.action?idnew=<s:property value="id"/>">Read More -></a>
  	      	</div>
  	      </div>
  	    </div>
	 </s:iterator>	
  
  <ul id="pagination-demo"class="pagination-sm"></ul>
</div>