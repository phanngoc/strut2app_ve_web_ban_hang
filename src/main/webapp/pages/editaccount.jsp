<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="container" id="editaccount">
	<div class="row">
	  <form>
	  	<div class="form-group">
	        <label for="inputUsername">Tên đăng nhập:</label>
	        <input type="text" class="form-control" id="inputUsername" placeholder="Username" name="username">
	    </div>
	    <div class="form-group">
	        <label for="inputPassword">Mật khẩu</label>
	        <input type="password" class="form-control" id="inputPassword" placeholder="Password" name="password">
	    </div>
	    <div class="form-group">
	        <label for="inputFullname">Tên đầy đủ</label>
	        <input type="text" class="form-control" id="inputFullname" placeholder="Fullname" name="fullname">
	    </div>
	    <div class="form-group">
	        <label for="inputEmail">Email</label>
	        <input type="email" class="form-control" id="inputEmail" placeholder="Email" name="email">
	    </div>
	    <div class="form-group">
	        <label for="inputAddress">Địa chỉ:</label>
	        <input type="text" class="form-control" id="inputAddress" placeholder="Address" name="address">
	    </div>
		<div class="form-group">
	        <label for="inputPhone">Điện thoại:</label>
	        <input type="text" class="form-control" id="inputPhone" placeholder="Phone" name="phone">
	    </div>
	    <button type="submit" class="btn btn-primary">Edit</button>
	  </form>
	</div>
	
	<div class="row">
		<table  class="table" >
		<caption>Các đơn hàng đã thực hiện</caption>
		    <thead>
		        <tr>
		            <th data-field="id">ID đơn chuyển</th>
		            <th data-field="dateship">Ngày chuyển</th>
		            <th data-field="expectship">Ngày sẽ nhận</th>
		        </tr>
		    </thead>
		    <tbody>
		    	   <s:iterator value="orders">
		    	   	<tr>
		    	    	<td><s:property value="id" /></td>
		    	     	<td><s:date name="dateship" format="dd/MM/yyyy" /></td>
		    	     	<td><s:date name="expectship" format="dd/MM/yyyy" /></td>
		    	    </tr>
	       		   </s:iterator>
		    </tbody>
		</table>
	</div>
	
	<div class="row">
		<table class="table">
		<caption>Các mặt hàng đã mua gần đây</caption>
		    <thead>
		        <tr>
		            <th data-field="id">Tên hàng</th>
		            <th data-field="name">Đơn giá</th>
		            <th data-field="price">Số lượng</th>
		        </tr>
		    </thead>
		    <tbody>
		    	   <s:iterator value="goodetailorders">
		    	   	<tr>
		    	    	<td><s:property value="good.name" /></td>
		    	     	<td><s:property value="good.price" /></td>
		    	     	<td><s:property value="detailorder.count" /></td>
		    	    </tr>
	       		   </s:iterator>
		    </tbody>
		</table>
	</div>
</div>