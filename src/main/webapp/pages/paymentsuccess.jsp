<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div id="paymentsuccess">
    <article>
        <div class="padding-header">
            <div class="center">
                <h3>Đơn đặt hàng của bạn đã được lưu thành công</h3>
                <p class="message"> Cảm ơn bạn đã dùng sản phẩm của chúng tôi,hàng của bạn sẽ được chuyển trong thời gian tới </p>
                <button ><a href="http://localhost:8080/phanbom1/main.action">Tiếp tục mua hàng</a></button>
            </div>
        </div>
    </article>
    
</div>