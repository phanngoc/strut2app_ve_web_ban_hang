<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div id="area-login" >
    <article>
        <div class="header-top"></div>
        <h2>Đăng kí</h2>
        <div id="content-login">
            <div id="wrap-center">
               <s:fielderror />
               <form method="POST" action="" class="container">
                <div class="row">
                 <div class="left col-md-6" >
				 	<div class="label">Tên đăng nhập:</div>
				 	 <br/>
		             <input type="text" name="user.username" />
		             <br/>
		             
		             <div class="label">Tên đầy đủ:</div>
		             <br/>
		             <input type="text" name="user.fullname" />
		             <br/>
		             
		             <div class="label">Mật khẩu:</div>
		             <br/>
		             <input type="text" name="user.password" />
		             <br/>
		             
		             <div class="label">Xác nhận mật khẩu:</div>
		             <br/>
		             <input type="text" name="user.confirmpassword" />
		             <br/>
		             
					 <button>Đăng Kí</button>	
				 </div>
	             <div class="right col-md-6">
	             	 <div class="label">Địa chỉ email:</div>
	             	 <br/>
		             <input type="text" name="user.email" />
		             <br/>
		             
		             <div class="label">Số điện thoại:</div>
		             <br/>
		             <input type="text" name="user.phone" />
		             <br/>
		             
		             <div class="label">Địa chỉ cư trú</div>
		             <br/>
		             <input type="text" name="user.address" />
		             <br/>
	             </div>
	             </div>
           		</form>
            </div>
        <div class="header-bot"></div>    
        </div>
    </article>
    
</div>