<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script type="text/javascript">
  WebFontConfig = {
    google: { families: [ 'Roboto+Slab::latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })(); </script>
<article id="detailarticle" class="container" style="width:780px;">
  <div class="row">
  	<h4><s:property value="news.title" /></h4>
  </div>
  <div class="row">
  	<p><s:date name="news.composeday" format="dd/MM/yyyy" /></p>
  </div>
  <div class="row contentde">
     <s:property value="news.content" />
  </div>
</article>