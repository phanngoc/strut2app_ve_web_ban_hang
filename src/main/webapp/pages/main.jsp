 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <header>
   	<h1 style="font-size:150%">I. Hệ thống các siêu thị hiện tại của Hoàn Long </h1>
 </header>
   	  <div class="content">

		   Hoanlong Cống Quỳnh: 244 Cống Quỳnh, P. Phạm Ngũ Lão, Q.1, Tp Hồ Chí Minh
		   Hoàn Long Nguyễn Thị Minh Khai: 410 B-C-D Nguyễn Thị Minh Khai, P.5, Q.3, Tp.HCM
		   Hoàn Long Gò Vấp: 15 Nguyễn Oanh, P.10, Q.Gò Vấp, Tp.HCM
		   II. Hoàn Long Computer sự lựa chọn đáng tin cậy:

			Để mua một chiếc máy tính, khách hàng thường gặp không ít khó khăn khi phải lựa chọn nơi thực sự uy tín, chất lượng cùng chế độ hậu mãi tốt, trước hàng trăm doanh nghiệp đã và đang tham gia thị trường bán lẻ các mặt hàng công nghệ như hiện nay. Việc đặt niềm tin đúng chỗ sẽ mang lại cho khách hàng một sản phẩm ưng ý về mọi mặt. Một trong những nhà bán lẻ máy tính và điện thoại di động uy tín hàng đầu tại TP.HCM phải kể đến là Công ty Cổ phần Máy tính Hoàn Long (Hoanlong Computer).
			
			Thương hiệu uy tín
			
			Công ty cổ phần máy tính Hoàn Long được thành lập năm 1993, chặng đường 19 năm phát triển đã đưa thương hiệu Hoàn Long trở thành một trong những nhà bán lẻ hàng đầu về sản phẩm công nghệ thông tin. Với nhiều thành tích, nỗ lực và uy tín trong suốt chiều dài kinh doanh công ty Hoàn Long đã được các cơ quan quản lý nhà nước, các hiệp hội uy tín trao tặng những bằng khen, huân chương điển hình như: 5 năm liền (2008, 2009, 2010, 2011, 2012 ) dành giải top 5 ICT VN và huy chương vàng ICT của hội tin học Tp.HCM, doanh nghiệp vàng năm 2008-2009, doanh nghiệp thương mại – dịch vụ tiêu biểu, nhà bán lẻ hàng đầu do bộ công thương hiệp hội doanh nghiệp vừa và nhỏ trao tặng….
			
			Hoàn Long tự hào chiếm khoảng 15% thị phần trong cả nước, với doanh thu khoảng 1.200 tỷ đồng mỗi năm, đang là đối tác chiến lược quan trong của những tập đoàn công nghệ nổi tiếng thế giới như tập đoàn Intel, Acer, HP, Q.Mobile, DELL, ASUS, VAIO, NOKIA, SAMSUNG…đó cũng là những bằng chứng tốt nhất cho việc đảm bảo uy tín của Hoàn Long đối với khách hàng.
			
			Với những cố gắng không ngừng giờ đây Hoàn Long Computer đã xây dựng cho mình được 7 cơ sở  lớn ở Tp.HCM và Cần Thơ, Đồng Nai và Đà Nẵng chuyên kinh doanh các trang thiết bị CNTT và ĐTDĐ, với đầy đủ chủng loại các mặt hàng, giá cả cạnh tranh.
			
			Sản phẩm tốt -  Giá cả hợp lý
			
			Hiện tại Hoàn Long Computer đang kinh doanh hơn 500 mặt hàng CNTT như: máy tính, linh kiện, thiết bị điện tử và điện thoại di dộng… với thương hiệu uy tín, sản phẩm tốt, giá hợp lý đã giúp Hoàn Long Computer trở thành một trong những nhà bán lẻ hàng đầu Việt Nam, được hàng nghìn người đến tham quan và mua sắm mỗi ngày.
			
			Song song với việc thực hiện các chương trình khuyến mãi hấp dẫn, Hoàn Long Computer đã chủ động đàm phán với các nhà cung cấp, các đối tác nhằm có những mức hỗ trợ giảm giá tối đa, giúp khách hàng mua sắm sao cho hiệu quả nhất. Vì vậy khi khách hàng đến với Hoàn Long Computer vừa có cơ hội sở hữu những sản phẩm chính hãng với chất lượng vượt trội của các thương hiệu nổi tiếng vừa được trải nghiệm dịch vụ chu đáo, tận tình của nhà bán lẻ hàng đầu mà không phải lo về giá cả.
			
			Chế độ hậu mãi tốt
			
			Khách hàng khó có thể kiểm tra chất lượng máy tính khi mua tại cửa hàng, do đó đa số khách hàng đặt quan tâm nhiều đến chế độ bảo hành của nhà bán lẻ. Với quyết tâm đảm bảo quyền lợi tối đa cho người tiêu dùng, Hoàn Long Computer đã xây dựng và cho ra đời chế độ bảo hành máy tính chỉ trong thời gian 4 giờ và dịch vụ bảo hành mở rộng, đảm bảo mang lại sự nhanh chóng, tiện lợi và an tâm tuyệt đối cho khách hàng khi mua sắm sản phẩm và sử dụng dịch vụ bảo hành, sửa chữa tại đây. Ngoài ra, Hoàn Long Computer cam kết sẽ đổi mới cho những sản phẩm được bảo hành hơn 4 giờ (nội dung được quy định tại website hoanlong.com.vn).
			
			Đồng thời với đội ngũ nhân viên chuyện nghiệp, phong cách phục vụ tận tình , ân cần và chu đáo, sẽ mang lại niềm tin tuyệt đối cho khách hàng khi đến với thương hiệu máy tính Hoàn Long.
			
			Khuyến mãi hấp dẫn
			
			Với phương châm luôn đồng hành cùng khách hàng trong thời kỳ kinh tế khó khăn như hiện nay, Hoàn Long Computer quyết định giảm lợi nhuận của chính mình để xây dựng cho khách hàng ngày càng nhiều chương trình khuyến mãi hấp dẫn, tiết kiệm được khoảng chi phí lớn trong việc mua sắm các thiết bị CNTT.
			
			Tất cả những chương trình khuyến mãi thiết thực đem lại hiệu quả đã ấp dẫn và thu hút được một lượng lớn khách hàng mỗi khi triển khai. Đa số các chương trình khuyến mãi là giảm giá, bốc thăm mua giá rẻ, quà tặng hấp dẫn… tạo cho khách hàng nhiều cơ hội sở hữu sản phẩm của Hoàn Long Computer với giá rẻ nhất.
			
			Hoàn Long Computer cam kết: Tất cả vì lợi ích của khách hàng.
   	  </div>	