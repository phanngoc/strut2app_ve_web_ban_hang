<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div id="area-login">

    <article>
        <h2>Đăng nhập</h2>
        <div id="content-login">
            <div id="wrap-center">
               <form method="POST" action="" >
            	 <s:actionmessage />
            	 
	             <div class="label">Tên đăng nhập:</div>
	             <input type="text" name="user.username" />
	             <br/>
	             
	             <div class="label">Mật khẩu:</div>
	             <input type="text" name="user.password" />
	             <br/>
	             
	             <a href="#" class="losepass">
	             <img src="<s:url value="/img/forgetpassword.png"/>" style="width:38px;height:38px" /> Quên mật khẩu</a>
	             <br/>
	             <button class="btn-login">Đăng nhập</button>
	             <p>Hoặc</p>
				 <button class="btn-register">Đăng Kí</button>	
           		</form>
            </div>
        <div class="header-bot"></div>    
        </div>
    </article>
    
</div>