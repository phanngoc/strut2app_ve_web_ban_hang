<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="showcart">
    <form action="" method="POST" class="container"> 
   <table class="row">
    <caption>Các hàng hoá đã thêm vào giỏ hàng</caption>
        <thead>
   	      <tr>
   	        <td>Tên sản phẩm</td>
   	        <td>Đơn giá</td>
   	        <td>Số lượng</td>
   	        <td>Tổng</td>
   	      </tr>
   	</thead>
   	  <c:if test="${listgood.size() > 0}" >
        <s:iterator value="listgood">
   	    <tr>
	    	<td><a href="#"></a><s:property value="name"/></a></td>
	    	<td><s:property value="price"/></td>
	    	<td>
                <input name="cart[<s:property value="id" />]" title="SL" class="count-cart" type="number" size="4" min="0" step="1" value="<s:property value="count"/>"/>
	    	</td>
	    	<td><s:property value="tong"/></td>
	    </tr>
	   </s:iterator>
      </c:if>
   	 <c:if test="${listgood.size() == 0}" >
   	   <tr>Bạn vẫn chưa chọn bất cứ sản phẩm nào</tr>
   	 </c:if>
   	 
	
   </table>
   <div class="event row">
      <div class="col-md-6"></div>
      <div  class="col-md-3">
      	<input  class="btn btn-info" type="submit" value="Cập nhật giỏ hàng" />
      </div>
      <div  class="col-md-3">
      
      	 <c:if test="${listgood.size() > 0}" >
	   	   <a   class="btn btn-info" href="http://localhost:8080/phanbom1/order.action" >Thanh toán</a>
	   	 </c:if>
          
      </div>
      
   </div>
   </form>
</div>
