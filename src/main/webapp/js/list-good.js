/**
 * 
 */
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
$(document).ready(function(){
   $(".addcart").click(function(){
	   $.ajax({
		   url:"showcart.action?id_good="+$(this).parent().prev().prev().prev().prev().val(),
		   success:function(result){
		     $("#div1").html(result);
		     console.log(getUrlVars()['id_type']);
		     window.location.replace("cart.action?id_type="+getUrlVars()['id_type']+"&id_cate="+getUrlVars()['id_cate']);
		   }
	   });
   });
});