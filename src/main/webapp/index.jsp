<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="Mua hang online">
<meta name="keywords" content="laptop,dien thoai,may tinh">
<meta name="author" content="bombay">


<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>


<s:iterator value="css">
		 <link rel="stylesheet" href="/phanbom1/css/<s:property />.css" />
</s:iterator>
<s:iterator value="js">
		 <script type="text/javascript" src="/phanbom1/js/<s:property />.js" ></script>
</s:iterator>

<script type="text/javascript">

</script>	
<title><s:property value="title"/></title>
</head>
<body>
    
	<jsp:include page="pages/header.jsp"></jsp:include>
	<div id="sidebar-top" >
	  <nav class="container">
	    <div class="col-md-10">
	    	 <div class="content-run">
	       		<ul>
	       		   <s:iterator value="listypes">
	       		     <li><a href="list.action?id_cate=1000&id_type=<s:property value="id" />"><s:property value="name" /></a></li>
	       		   </s:iterator>
	       		</ul>	
	   		 </div>
	    </div>
	    
	    <div class="col-md-2">
	       <div class="container">
	        <div class="row">
			    <form action="" method="GET" id="search">
			 	  <input type="text" name="search" class="field"  placeholder="Search"/>
			 	  <input type="submit" class="submit" name="submit" id="searchsubmit" value="Search" style="display:none">
			    </form>
		    </div>
		    <div id="status row">
	            <div class="area-login" style="margin-bottom:4px;">
	            <c:if test="${!storeuser.checkUser()}">
	           	  <a href="login.action"> Đăng nhập </a>
	            </c:if>
	            <c:if test="${storeuser.checkUser()}">
	           	  <img src="/phanbom1/img/avatar.png" style="width:38px;height:38px;"/><a href="editaccount.action"> <c:out value="${storeuser.getUser().getUsername()}"/> </a>
	           	  <a href="logout.action">logout</a>
	            </c:if>    
	            </div>
	            <div class="area-cart">
	                <a href="cart.action"> <img src="/phanbom1/img/shoping_cart.png" style="width:38px;height:38px;"/>Xem giỏ hàng</a>
	            </div>
            </div>
		    </div>
		</div>
		
	  </nav>
	</div>
        
	<div id="wrap-main">
	<aside class="sidebar-left">
	  <h4>Các thể loại nên chọn</h4>
	  		<table>
		  		<s:iterator value="listcates">
		  			<tr>
					  <td><a href="list.action?id_cate=<s:property value="id"/>&id_type=<s:property value="id_type"/>"><s:property value="name" /></a></td>
					  <td>>></td> 
					</tr>  
	   			</s:iterator>

			</table>
	</aside>
	
	
	<article class="article-center">
	  <s:include value="pages/%{page}.jsp">
	  	<s:param name="listgood" value="%{listgood}"></s:param>
	  </s:include>
	</article>
	
	</div>
	
	<jsp:include page="pages/footer.jsp"></jsp:include>
</body>
</html>
