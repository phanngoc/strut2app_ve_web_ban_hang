package phanbom1;
 
import com.opensymphony.xwork2.conversion.TypeConversionException;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;
import org.apache.struts2.util.StrutsTypeConverter;
 
 
public class ShortArticleConverter extends StrutsTypeConverter {
 
    @Override
    public Object convertFromString(Map map, String[] strings, Class type) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
 
    @Override
    public String convertToString(Map map, Object obj) {
        try {
        	System.out.println("convertToString:"+obj);
            String str = (String) obj;
            str = str.substring(0,100)+"...";
            return str;
        } catch (Exception e) {
            throw new TypeConversionException(e);
        }
    }
}