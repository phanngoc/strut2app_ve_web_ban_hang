package phanbom1;

import java.util.ArrayList;
import java.util.List;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.conversion.annotations.TypeConversion;

import model.Category;
import model.DataManager;
import model.MoCate;
import model.News;
import model.Type;

public class NewsList extends ActionSupport implements ModelDriven{
 public DataManager manager = new DataManager();
 public ArrayList<Category> listcates = new ArrayList<Category>();
 public MoCate mocate = new MoCate();
 public String page = "list-news";
 public int totalPages;
 private ArrayList<News> news = new ArrayList<News>();
 private List<News> newshow; 
 
 private int indexpage=1;
   public String execute() 
   { 
	   mocate.addCss("list-news1");
	   
	   mocate.addJs("list-news");
	   mocate.addJs("jquery.twbsPagination");
	   
	   listcates = manager.getListCategory(1);
	   ArrayList<Type> listtypes = manager.getListType();

	   mocate.setListcates(listcates); 
	   mocate.setListypes(listtypes);
	   mocate.setPage(page);
       String ret = SUCCESS;  
       news = manager.getNews();
       //if(indexpage)
      // newshow = news.subList(indexpage*5, (indexpage+1)*5);
       int to = (indexpage*5 >= news.size()) ? news.size()-1 : (indexpage*5); 
       newshow = news.subList((indexpage-1)*5,to);
       totalPages = (news.size()%5==0) ? news.size()/5 : news.size()/5+1; 
      System.out.print("so luong"+listcates.size());
      return ret;
   }

	public Object getModel() {
		// TODO Auto-generated method stub
		return mocate;
	}	
	public void setNews(ArrayList<News> news)
	{
		this.news = news;
	}
	public List<News> getNews()
	{
		return this.news;
	}

	public void setNewshow(List<News> news)
	{
		this.newshow = news;
	}
	public List<News> getNewshow()
	{
		return this.newshow;
	}
	public int getIndexpage()
	{
		return indexpage;
	}
	public void setIndexpage(int indexpage)
	{
		this.indexpage = indexpage;
	}
	public int getTotalPages()
	{
		return totalPages;
	}
	public void setTotalPages(int totalpage)
	{
		this.totalPages = totalpage;
	}
}