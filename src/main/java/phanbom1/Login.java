/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanbom1;

/**
 *
 * @author phann123
 */




import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.HashMap;

import model.Category;
import model.DataManager;
import model.MoCate;
import model.Shop;
import model.Type;
import model.Item;
import model.User;
import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.util.logging.LoggerFactory;

public class Login extends ActionSupport implements ModelDriven,SessionAware,ServletRequestAware{
	
 private static final Logger LOG = LoggerFactory.getLogger(Login.class);
 public DataManager manager = new DataManager();
 public ArrayList<Item> listgood = new ArrayList<Item>();
 Shop shop;
 public MoCate mocate = new MoCate();
 public int id_cate = -1;
 Map<String, Object> session;
 boolean flag_init = false;
 User user = new User();
 HttpServletRequest request;
   public String execute() 
   {  
     
       mocate.addCss("login");
       ArrayList<Category> listcates = manager.getListCategory(1);
	   ArrayList<Type> listtypes = manager.getListType();

	   mocate.setListcates(listcates);
	   mocate.setListypes(listtypes);
       System.out.print("So cate la:"+listcates.size());
           
       mocate.setPage("login");
  
       return SUCCESS;
   }

   
	public Object getModel() {
		// TODO Auto-generated method stub
		return mocate;
	}
	public void setMocate(MoCate mode)
	{
		this.mocate = mode;
	}
	public MoCate getMocate()
	{
		return this.mocate;
	}
	public int getIdCate()
	{
		return this.id_cate;
	}
	public void setIdCate(int id)
	{
		this.id_cate = id;
	}
	public ArrayList<Item> getListgood()
	{
		return this.listgood;
	}
	public void setListgood(ArrayList<Item> goods)
	{
		this.listgood = goods;
	}

	public void setSession(Map<String, Object> arg0) {
		// TODO Auto-generated method stub
		session = arg0;
	}
    public void setUser(User user)
    {
    	this.user = user;
    }
    public User getUser()
    {
    	return this.user;
    }
    public void validate() {
    	System.out.print("Dang o trong validate"+user.toString());
    	String method = this.request.getMethod() ;
    	boolean error = false;
        if(method.equals("POST"))
        {
        	if(user.getUsername().equals(""))
        	{
        		addActionMessage("Bạn chưa điền tên đăng nhập.");	
        		error = true;
        	}
        	if(user.getPassword().equals(""))
        	{
        		addActionMessage("Bạn chưa điền mật khẩu.");
        		error = true;
        	}
        	if(!error)
        	{
        		Utils.write("no error");
        		User us = manager.getUser(user.getUsername(), user.getPassword());
        		if(us==null)
        		{
        			addActionMessage("Tên đăng nhập hoặc mật khẩu không đúng");
        		}
        		else
        		{
        			Utils.write("Username va password correct");
        			mocate.getStoreuser().setUser(us);
        			HttpServletResponse response=(HttpServletResponse) ActionContext.getContext().get(ServletActionContext.HTTP_RESPONSE);
        		    String url="http://localhost:8080/phanbom1/main.action";
        		    try {
						response.sendRedirect(url);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
        		}	
        			
        	}
    		System.out.print("Co vao");
        }
               //addFieldError("user.username","Bạn phải điền username");
        	
    
    		
    	System.out.print("Dang check"+user.getUsername());
    }


	public void setServletRequest(HttpServletRequest request) {
		// TODO Auto-generated method stub
		this.request = request;
	}
	
}
