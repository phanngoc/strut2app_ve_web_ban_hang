package phanbom1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Category;
import model.CombineGoodDetailOrder;
import model.DataManager;
import model.MoCate;
import model.News;
import model.Order;
import model.Type;
import model.User;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class ArticleDetail extends ActionSupport implements ModelDriven{
	public DataManager manager = new DataManager();
	public MoCate mocate = new MoCate();
	public int idnew = 1; 
	public News news;
	   public String execute() 
	   {  
		   mocate.addCss("articledetail");
	       ArrayList<Category> listcates = manager.getListCategory(1);
		   ArrayList<Type> listtypes = manager.getListType();

		   mocate.setListcates(listcates);
		   mocate.setListypes(listtypes);
   
	       mocate.setPage("articleDetail");
	       news = manager.getNews(idnew);
	       return SUCCESS;
	   }
	    public Object getModel() {
			// TODO Auto-generated method stub
			return mocate;
		}
		public void setMocate(MoCate mode)
		{
			this.mocate = mode;
		}
		public MoCate getMocate()
		{
			return this.mocate;
		}
		
		public int getIdnew()
		{
			return idnew;
		}
		public void setIdnew(int idnew)
		{
			this.idnew = idnew;
		}
		public void setNews(News news)
		{
			this.news = news;
		}
		public News getNews()
		{
			return this.news;
		}

}
