package phanbom1;

import java.lang.reflect.Field;
import java.util.ArrayList;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import model.Category;
import model.DataManager;
import model.Good;
import model.MoCate;
import model.Type;

public class ShowList extends ActionSupport implements ModelDriven{
 public DataManager manager = new DataManager();
 public ArrayList<Good> listgood = new ArrayList<Good>();
 public MoCate mocate = new MoCate();
 public int id_cate = -1;
 
   public String execute() 
   {  
	   mocate.addCss("list-good1");
	   mocate.addJs("list-good");
	   
       String ret = SUCCESS; 
       
       if(id_cate==1000)
       {
    	   id_cate = manager.getFirstCategory(mocate.getIdType()).getId();   
       }
       
       ArrayList<Category> listcates = manager.getListCategory(mocate.getIdType());
       ArrayList<Type> listtypes = manager.getListType();
       System.out.print("listcates count:"+listcates.size()+",mocate.getIdType():"+mocate.getIdType());

	   mocate.setListcates(listcates);
	   mocate.setPage("list-good");
	   mocate.setListypes(listtypes);
       listgood = manager.getGoodBelongCate(id_cate);
       System.out.print("List good :"+listgood.size());
      //Field [] attributes =  listgood.get(0).getClass().getDeclaredFields();
      //System.out.println("list good ne:"+attributes[0]+"|"+attributes[1]);
  
      return ret;
   }

	public Object getModel() {
		// TODO Auto-generated method stub
		return mocate;
	}
	public void setMocate(MoCate mode)
	{
		this.mocate = mode;
	}
	public MoCate getMocate()
	{
		return this.mocate;
	}
	public int getIdCate()
	{
		return this.id_cate;
	}
	public void setIdCate(int id)
	{
		this.id_cate = id;
	}
	public ArrayList<Good> getListgood()
	{
		return this.listgood;
	}
	public void setListgood(ArrayList<Good> goods)
	{
		this.listgood = goods;
	}
	
	
}