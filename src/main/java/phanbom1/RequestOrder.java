package phanbom1;

import static com.opensymphony.xwork2.Action.SUCCESS;

import com.opensymphony.xwork2.ActionContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import model.Category;
import model.DataManager;
import model.MoCate;
import model.Shop;
import model.Type;
import model.Item;
import model.User;

public class RequestOrder extends ActionSupport implements ModelDriven,SessionAware{
 public DataManager manager = new DataManager();
 Shop shop;
 public MoCate mocate = new MoCate();
 public int id_cate = -1;
 Map<String, Object> session;
 Map<Integer,Integer> cart = new HashMap<Integer,Integer>();
 ArrayList<Item> listgood;
   public String execute() 
   {  

	   mocate.addCss("requestOrder");
	   mocate.addJs("requestOrder");
           String ret = SUCCESS;  
           if(id_cate==1000)
           {
                id_cate = manager.getFirstCategory(mocate.getIdType()).getId();   
           }
       
           ArrayList<Category> listcates = manager.getListCategory(mocate.getIdType());
           ArrayList<Type> listtypes = manager.getListType();
      
	   mocate.setListcates(listcates);
	   mocate.setListypes(listtypes);
	   System.out.println("chuan bi check shop");
	   
           if(!mocate.getStoreuser().checkUser())
           {
        	  HttpServletResponse response=(HttpServletResponse) ActionContext.getContext().get(ServletActionContext.HTTP_RESPONSE);
 	          String url="http://localhost:8080/phanbom1/login.action";
   		      try {
						response.sendRedirect(url);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
           }
           else
           {
        	   User user = mocate.getStoreuser().getUser();
        	   Map<String,Object> session = ActionContext.getContext().getSession();
               if(session.containsKey("shop"))
               {
                   shop = (Shop)session.get("shop");
               }
        	   manager.insertOrder(user, shop);
               mocate.setPage("paymentsuccess");
               mocate.addCss("paymentsuccess");
               
           }
           return ret;
   }

   
	public Object getModel() {
		// TODO Auto-generated method stub
		return mocate;
	}
	public void setMocate(MoCate mode)
	{
		this.mocate = mode;
	}
	public MoCate getMocate()
	{
		return this.mocate;
	}
	public int getIdCate()
	{
		return this.id_cate;
	}
	public void setIdCate(int id)
	{
		this.id_cate = id;
	}
	public ArrayList<Item> getListgood()
	{
		return this.listgood;
	}
	public void setListgood(ArrayList<Item> goods)
	{
		this.listgood = goods;
	}
	public void setShop(Shop shoptem)
	{
		this.shop = shoptem;
	}
	public Shop getShop()
	{
		return this.shop;
	}
	public void setSession(Map<String, Object> arg0) {
		// TODO Auto-generated method stub
		session = arg0;
	}
        
	public void setCart(Map<Integer,Integer> cartt)
        {
            this.cart = cartt;
        }
        
        public Map<Integer,Integer> getCart()
        {
            return this.cart;
        }
	
}