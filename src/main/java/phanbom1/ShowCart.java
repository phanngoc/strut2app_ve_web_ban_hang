package phanbom1;

import java.util.ArrayList;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.HashMap;

import model.Category;
import model.DataManager;
import model.MoCate;
import model.Shop;
import model.Type;
import model.Item;

public class ShowCart extends ActionSupport implements ModelDriven,SessionAware{
 public DataManager manager = new DataManager();
 public ArrayList<Item> listgood = new ArrayList<Item>();
 Shop shop;
 public MoCate mocate = new MoCate();
 public int id_cate = -1;
 Map<String, Object> session;
 Map<Integer,Integer> cart = new HashMap<Integer,Integer>();

   public String execute() 
   {  
     

	   mocate.addCss("showcart");
	   mocate.addJs("showcart");
	   
           String ret = SUCCESS;  
           if(id_cate==1000)
           {
                id_cate = manager.getFirstCategory(mocate.getIdType()).getId();   
           }
       
           ArrayList<Category> listcates = manager.getListCategory(mocate.getIdType());
           ArrayList<Type> listtypes = manager.getListType();
      

	   mocate.setListcates(listcates);
	   mocate.setPage("ShowCart");
	   mocate.setListypes(listtypes);
	   System.out.println("chuan bi check shop");
	   if (session.containsKey("shop"))
           {
		   		System.out.println("co shop trong session");
		   		shop=(Shop)session.get("shop");
                if(cart.size()!=0)
                {
                   System.out.println("So item cap nhat den cart la:"+cart.size());
                   shop.updateCountGood(cart);
                }
                
                listgood = shop.getItem();
                System.out.println("so item trong shop:"+listgood.size());
           }
           
        return ret;
   }

   
	public Object getModel() {
		// TODO Auto-generated method stub
		return mocate;
	}
	public void setMocate(MoCate mode)
	{
		this.mocate = mode;
	}
	public MoCate getMocate()
	{
		return this.mocate;
	}
	public int getIdCate()
	{
		return this.id_cate;
	}
	public void setIdCate(int id)
	{
		this.id_cate = id;
	}
	public ArrayList<Item> getListgood()
	{
		return this.listgood;
	}
	public void setListgood(ArrayList<Item> goods)
	{
		this.listgood = goods;
	}
	public void setShop(Shop shoptem)
	{
		this.shop = shoptem;
	}
	public Shop getShop()
	{
		return this.shop;
	}
	public void setSession(Map<String, Object> arg0) {
		// TODO Auto-generated method stub
		session = arg0;
	}
        
	public void setCart(Map<Integer,Integer> cartt)
        {
            this.cart = cartt;
        }
        
        public Map<Integer,Integer> getCart()
        {
            return this.cart;
        }
	
}