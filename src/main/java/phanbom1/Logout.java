package phanbom1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import model.Category;
import model.MoCate;
import model.Type;
import model.User;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class Logout extends ActionSupport{
	public MoCate mocate = new MoCate();
	 HttpServletRequest request;
	   public String execute() 
	   {  
	       mocate.getStoreuser().logout();
	       HttpServletResponse response=(HttpServletResponse) ActionContext.getContext().get(ServletActionContext.HTTP_RESPONSE);
		    String url="http://localhost:8080/phanbom1/main.action";
		    try {
				response.sendRedirect(url);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       return SUCCESS;
	   }

}
