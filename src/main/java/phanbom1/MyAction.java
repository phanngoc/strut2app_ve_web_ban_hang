package phanbom1;

import java.util.Map;

import com.opensymphony.xwork2.ActionSupport;
import javax.servlet.http.HttpSession;

import model.DataManager;
import model.Shop;
import model.Good;
import org.apache.struts2.interceptor.SessionAware;

public class MyAction extends ActionSupport implements SessionAware {
      public DataManager manager = new DataManager();
	  // For SessionAware
      Map<String, Object> session;
      public Integer id_good;
      
      public void setIdGood(int id_good)
      {
    	  this.id_good = id_good;
      }
      
      public Integer getIdGood()
      {
    	  return this.id_good;
      }
      
      public void setSession(Map<String, Object> session) {
	    this.session = session;
      }
	  public String execute() {
		  Shop shop;
		  System.out.println("id_good"+id_good);
          Good good = manager.getGoodFromId(id_good);
		  if (session.containsKey("shop"))
		  {
			  shop=(Shop)session.get("shop");
		  }
		  else
		  {
			  shop = new Shop();
		  }
	          shop.addItem(good);
		  session.put("shop",shop);
		  
		  return "success";
	 }
}