/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanbom1;

/**
 *
 * @author phann123
 */


import static com.opensymphony.xwork2.Action.SUCCESS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Category;
import model.DataManager;
import model.MoCate;
import model.Shop;
import model.Type;
import model.Item;
import model.User;

public class Register extends ActionSupport implements ModelDriven,SessionAware,ServletRequestAware{
 public DataManager manager = new DataManager();
 public ArrayList<Item> listgood = new ArrayList<Item>();
 Shop shop;
 public MoCate mocate = new MoCate();
 public int id_cate = -1;
 Map<String, Object> session;
 User user = new User();
 HttpServletRequest request;
 public Register()
 {
	 Utils.write("Contructs Register");
	 mocate.addCss("register");
     
     if(id_cate==1000)
     {
          id_cate = manager.getFirstCategory(mocate.getIdType()).getId();   
     }
 
     ArrayList<Category> listcates = manager.getListCategory(mocate.getIdType());
     ArrayList<Type> listtypes = manager.getListType();

 mocate.setListcates(listcates);
 mocate.setListypes(listtypes);
     
 mocate.setPage("register");
 }
   public String execute() 
   { 
	   Utils.write("execute Register");
       return SUCCESS;
   }
   
   public void validate()
   {
	   String method = this.request.getMethod() ;
       boolean error = false;
       if(method.equals("POST"))
       {
    	   if (user.getUsername().trim().equals(""))
    	      {
    		     addFieldError("user.username","Bạn phải điền username");
    	         error = true;
    	      }
    	      if (user.getFullname().equals(""))
    	      {
    	    	  addFieldError("user.fullname","Bạn phải điền fullname");
    	          error = true;
    	      }
    	      if (user.getAddress().equals(""))
    	      {
    	    	  addFieldError("user.address","Bạn phải điền địa chỉ chi tiết.");
    	          error = true;
    	      }
    	      if (!user.getPassword().equals(user.getConfirmpassword()))
    	      {
    	    	  addFieldError("user.confirmpassword","Hai password không giống nhau");
    	    	  error = true;
    	      }
    	      
    	      String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
    	      
    	      Boolean b = user.getEmail().matches(EMAIL_REGEX);
    	      if(!b){
    	    	  addFieldError("user.email","Đây không phải email hợp lệ"); 
    	    	  error = true;
    	      }
    	      String expression = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$";  
    	      CharSequence inputStr = user.getPhone();  
    	      Pattern pattern = Pattern.compile(expression);  
    	      Matcher matcher = pattern.matcher(inputStr);  
    	      if(!matcher.matches()){  
    	    	  addFieldError("user.phone","Số điện thoại không đúng");
    	    	  error = true;
    	      }  
    	      if(!error)
    	      {
    	    	  Utils.write("no error");
    	    	  manager.insertUser(user);
    	    	  mocate.getStoreuser().setUser(user);
    	    	  HttpServletResponse response=(HttpServletResponse) ActionContext.getContext().get(ServletActionContext.HTTP_RESPONSE);
    	          String url="http://localhost:8080/phanbom1/main.action";
      		      try {
						response.sendRedirect(url);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    	      }
       }
       
   }
   
	public Object getModel() {
		// TODO Auto-generated method stub
		return mocate;
	}
	public void setUser(User user)
    {
    	this.user = user;
    }
    public User getUser()
    {
    	return this.user;
    }
	public void setMocate(MoCate mode)
	{
		this.mocate = mode;
	}
	public MoCate getMocate()
	{
		return this.mocate;
	}
	public int getIdCate()
	{
		return this.id_cate;
	}
	public void setIdCate(int id)
	{
		this.id_cate = id;
	}
	public ArrayList<Item> getListgood()
	{
		return this.listgood;
	}
	public void setListgood(ArrayList<Item> goods)
	{
		this.listgood = goods;
	}
	public void setShop(Shop shoptem)
	{
		this.shop = shoptem;
	}
	public Shop getShop()
	{
		return this.shop;
	}
	public void setSession(Map<String, Object> arg0) {
		// TODO Auto-generated method stub
		session = arg0;
	}

	public void setServletRequest(HttpServletRequest request) {
		// TODO Auto-generated method stub
		this.request = request;
	}
        

	
}
