/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanbom1;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import java.util.Map;

/**
 *
 * @author phann123
 */
public class SessionInterceptor extends AbstractInterceptor{
    
    @Override
    public String intercept(ActionInvocation invocation) throws Exception
    {
     System.out.println("Vao intercept");
     Map<String,Object> session = invocation.getInvocationContext().getSession();
        if(!session.containsKey("login"))
        {
            return "login";
        }
       
       return invocation.invoke();
    }
}
