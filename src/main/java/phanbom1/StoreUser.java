/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phanbom1;

import com.opensymphony.xwork2.ActionContext;
import java.util.Map;
import model.User;
/**
 *
 * @author phann123
 */
public class StoreUser {
    private static StoreUser storeuser = null;
    public User user;
    public String display;

    public static StoreUser getInstance()
    {  
        if(storeuser!=null)
        {
            return storeuser;
        }
        else
        {
            StoreUser storeuser = new StoreUser();
            return storeuser;
        }
    }
    public boolean checkUser()
    {
        Map<String,Object> session = ActionContext.getContext().getSession();
        if(session.containsKey("user"))
        {
            user = (User)session.get("user");
            return true;
        }
        else
        {
            return false;
        }
    }
    public User getUser()
    {
        return user;
    }
    public void setUser(User user)
    {
        this.user = user;
        Map<String,Object> session = ActionContext.getContext().getSession();
        session.put("user", user);
    }
    public void logout()
    {
        Map<String,Object> session = ActionContext.getContext().getSession();
        session.remove("user");
    }
}
