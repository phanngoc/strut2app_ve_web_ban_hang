package phanbom1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Category;
import model.CombineGoodDetailOrder;
import model.DataManager;
import model.MoCate;
import model.Order;
import model.Type;
import model.User;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class EditAccount extends ActionSupport implements ModelDriven{
	public DataManager manager = new DataManager();
	public MoCate mocate = new MoCate();
	 HttpServletRequest request;
	 ArrayList<Order> orders = new ArrayList<Order>();
	 ArrayList<CombineGoodDetailOrder> goodetailorders ;
	 User user;
	   public String execute() 
	   {  

		   mocate.addCss("editaccount");
	       ArrayList<Category> listcates = manager.getListCategory(1);
		   ArrayList<Type> listtypes = manager.getListType();

		   mocate.setListcates(listcates);
		   mocate.setListypes(listtypes);

	           
	       mocate.setPage("editaccount");
	       //user = mocate.getStoreuser().getUser();
	       Map<String,Object> session = ActionContext.getContext().getSession();
	       user = (User)session.get("user");

	      
	       orders = manager.getOrder(user.getId());   
	      
	       
	       System.out.println("Da lay getorder id user"+orders.size());
	       goodetailorders = manager.getItem(user.getId());
	       return SUCCESS;
	   }
	    public Object getModel() {
			// TODO Auto-generated method stub
			return mocate;
		}
		public void setMocate(MoCate mode)
		{
			this.mocate = mode;
		}
		public MoCate getMocate()
		{
			return this.mocate;
		}
		public ArrayList<Order> getOrders()
		{
			return orders;
		}
		public void setOrders(ArrayList<Order> orders)
		{
			this.orders = orders;
		}
		public ArrayList<CombineGoodDetailOrder> getGoodetailorders()
		{
			return goodetailorders;
		}
		public void setGoodetailorders(ArrayList<CombineGoodDetailOrder> goods)
		{
			this.goodetailorders = goods;
		}
		public User getUser()
		{
			return this.user;
		}
		public void setUser(User user)
		{
			this.user = user;
		}

}
