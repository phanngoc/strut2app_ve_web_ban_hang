package phanbom1;

import java.lang.reflect.Field;

public class Utils {
  // private static final Logger LOG = LoggerFactory.getLogger();
   public static void write(String text)
   {
	   System.out.println(text);
	//   LOG.debug("MyAction executed with UserName [#0]", userName);
   }
   public static String dump(Object object) {
       Field[] fields = object.getClass().getDeclaredFields();
       StringBuilder sb = new StringBuilder();
       sb.append(object.getClass().getSimpleName()).append('{');

       boolean firstRound = true;

       for (Field field : fields) {
           if (!firstRound) {
               sb.append(", ");
           }
           firstRound = false;
           field.setAccessible(true);
           try {
               final Object fieldObj = field.get(object);
               final String value;
               if (null == fieldObj) {
                   value = "null";
               } else {
                   value = fieldObj.toString();
               }
               sb.append(field.getName()).append('=').append('\'')
                       .append(value).append('\'');
           } catch (IllegalAccessException ignore) {
               //this should never happen
           }

       }

       sb.append('}');
       return sb.toString();
   }
}
