package model;

import java.awt.List;
import java.util.ArrayList;
import java.util.Date;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import phanbom1.Utils;
import model.CategoryDAO;

import java.math.BigInteger;

public class DataManager {
  public ArrayList<Category> lists = new ArrayList<Category>();
  public Session session = null;
  public ArrayList<Category> getListCategory(int id)
  { 
	  ArrayList<Category> cates = null;
	  session = CategoryDAO.getSessionFactory().getCurrentSession();
      session.beginTransaction();
      try { 
      cates = (ArrayList<Category>)session.createQuery("from Category where id_type="+id).list();       
      } catch (HibernateException e) {
          e.printStackTrace();
          session.getTransaction().rollback();
          throw e;
      }
      finally
      {
    	  if (session != null) {
    		 // session.getTransaction().commit();
    	      session.close();
    	  }
      }
      return cates;
  }
  
  public Good getGoodFromId(int id)
  {
	  Good good;
	  session = CategoryDAO.getSessionFactory().getCurrentSession();
      session.beginTransaction();
      try {
    	 good = (Good)session.createQuery("from Good where id="+id).list().get(0);       
      } catch (HibernateException e) {
          e.printStackTrace();
          session.getTransaction().rollback();
          throw e;
      }
      finally
      {
    	  if (session != null) {
    		 // session.getTransaction().commit();
    	      session.close();
    	  }
      }
      return good;
  }
  
  
  public Category getFirstCategory(int id)
  { 
	  ArrayList<Category> cates = null;
	  session = CategoryDAO.getSessionFactory().getCurrentSession();
      session.beginTransaction();
      try {
      cates = (ArrayList<Category>)session.createQuery("from Category where id_type="+id).list();       
      } catch (HibernateException e) {
          e.printStackTrace();
          session.getTransaction().rollback();
          throw e;
      }
      finally
      {
    	  if (session != null) {
    		 // session.getTransaction().commit();
    	      session.close();
    	  }
      }
      return cates.get(0);
  }
  
  
  
  public ArrayList<Good> getGoodBelongCate(int cate_id)
  { 
	  ArrayList<Good> goods = null;
	  System.out.print("getGoodBelongCate");
	  session = CategoryDAO.getSessionFactory().getCurrentSession();
      session.beginTransaction();
      try {
    	//String queryString = "select Good.id,Good.name,Good.price,Good.linkimage,Good.special_offer from Good,CateGood where Good.id = CateGood.idGood and CateGood.idCate = :numbercate";
    	//String queryString = "select model.Good.id,model.Good.name,model.Good.price,model.Good.linkimage,model.Good.special_offer from Good,CateGood where model.Good.id = model.CateGood.idGood and model.CateGood.idCate = :numbercate";
    	//  String queryString "select go from Good as go,CateGood where Good.id = CateGood.idGood and CateGood.idCate.id = :numbercate";
		Query query =  session.createQuery("select go from Good as go join go.cates as cat where cat.id=?").setInteger(0, cate_id);  
    	//query.setParameter("numbercate", cate_id) ;
    	goods = (ArrayList<Good>)query.list();
      } catch (HibernateException e) {
          e.printStackTrace();
          session.getTransaction().rollback();
          throw e;
      }
      finally
      {
    	  if (session != null) {
    		  //session.getTransaction().commit();
    	      session.close();
    	  }
      }
      return goods;
  }
  public ArrayList<Type> getListType()
  { 
	  ArrayList<Type> cates = null;
	  session = CategoryDAO.getSessionFactory().getCurrentSession();
      session.beginTransaction();
      try {
      cates = (ArrayList<Type>)session.createQuery("from Type").list();       
      } catch (HibernateException e) {
          e.printStackTrace();
          session.getTransaction().rollback();
          throw e;
      }
      finally
      {
    	  
    	  if (session != null) {
    		  //session.getTransaction().commit();
    	      session.close();
    	  }
      }
      
      return cates;
  }
  public ArrayList<News> getNews()
  { 
	  ArrayList<News> cates = null;
	  session = CategoryDAO.getSessionFactory().getCurrentSession();
      session.beginTransaction();
      try {
      cates = (ArrayList<News>)session.createQuery("from News").list();       
      } catch (HibernateException e) {
          e.printStackTrace();
          session.getTransaction().rollback();
          throw e;
      }
      finally
      {
    	  
    	  if (session != null) {
    		  //session.getTransaction().commit();
    	      session.close();
    	  }
      }
      
      return cates;
  }
  // Check user login
  public User getUser(String username,String password)
  { 
	  User user = null;
	  session = CategoryDAO.getSessionFactory().getCurrentSession();
      session.beginTransaction();
      try {
         ArrayList<User> listuser =  (ArrayList<User>)session.createQuery("from User where username='"+username+"' and password='"+password+"'").list();
         if(listuser.size()!=0)
         {
        	 user = listuser.get(0);
         }
      } catch (HibernateException e) {
          e.printStackTrace();
          session.getTransaction().rollback();
          throw e;
      }
      finally
      {
    	  
    	  if (session != null) {
    		  //session.getTransaction().commit();
    	      session.close();
    	  }
      }
      
      return user;
  }
  public void insertUser(User user)
  { 
	  session = CategoryDAO.getSessionFactory().getCurrentSession();
      session.beginTransaction();
      try {
    	  session.save(user);
    	  session.getTransaction().commit();
    	  Utils.write("Da commit");
    	  
      } catch (HibernateException e) {
    	  Utils.write("Exception :"+e);
          e.printStackTrace();
          //session.getTransaction().rollback();
          throw e;
      }
      finally 
      {
    	  Utils.write("finally");
    	  //if (session != null) {
    	//	  Utils.write("Khac null va close");
    	 //     session.close();
    	 // }
      }
  } 
  public void insertOrder(User user,Shop shop)
  { 
	  session = CategoryDAO.getSessionFactory().getCurrentSession();
      session.beginTransaction();
      try {
    	  Order order = new Order(new Date(), new Date(), user.getId(), 0); 
    	  session.save(order);
    	  Long lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).longValue();
    	  ArrayList<Item> listitem = shop.getItem();
    	  for(int i=0;i<listitem.size();i++)
    	  {
    		  Item item = listitem.get(i);
    		  Detailorder detailorder = new Detailorder(item.getId(),item.getCount(),lastId.intValue());
    		  session.save(detailorder);
    	  }
    	  session.getTransaction().commit();
    	  Utils.write("Da commit");
    	  
      } catch (HibernateException e) {
    	  Utils.write("Exception :"+e);
          e.printStackTrace();
          //session.getTransaction().rollback();
          throw e;
      }
      finally 
      {
    	  Utils.write("finally");
    	  //if (session != null) {
    	//	  Utils.write("Khac null va close");
    	 //     session.close();
    	 // }
      }
  }
  public ArrayList<Order> getOrder(int userid)
  { 
	  System.out.println("vao getOrder");
	  session = CategoryDAO.getSessionFactory().getCurrentSession();
	  System.out.println("get session");
      session.beginTransaction();
      System.out.println("bat dau transaction");
      try {
         ArrayList<Order> listorder =  (ArrayList<Order>)session.createQuery("from Order where userid="+userid).list();
         System.out.println("listorder size"+listorder.size());
         return listorder;
      } catch (HibernateException e) {
          e.printStackTrace();
          session.getTransaction().rollback();
          throw e;
      }
      finally
      {
    	  
    	  if (session != null) {
    		  //session.getTransaction().commit();
    	      session.close();
    	  }
      }
  }
  
  public ArrayList<CombineGoodDetailOrder> getItem(int userid)
  { 

	  ArrayList<CombineGoodDetailOrder>  combines = new ArrayList<CombineGoodDetailOrder>();
	  session = CategoryDAO.getSessionFactory().getCurrentSession();
      session.beginTransaction();
      try {

    	  ArrayList<Order> listorder =  (ArrayList<Order>)session.createQuery("from Order where userid="+userid).list();

          for(int i=0;i<listorder.size();i++)
          {
        	  int orderid = listorder.get(i).getId();

        	  ArrayList<Detailorder> listdetail =  (ArrayList<Detailorder>)session.createQuery("from Detailorder where orderid="+orderid).list();

        	  for(int j=0;j<listdetail.size();j++)
              {
        		  ArrayList<Good> listgood =  (ArrayList<Good>)session.createQuery("from Good where id="+listdetail.get(j).getGoodid()).list();
        		  if(listgood.size()>0)
        		  {
        			  CombineGoodDetailOrder combile = new CombineGoodDetailOrder(listgood.get(0),listdetail.get(j));
            		  combines.add(combile);  
        		  }
              }
          }
          System.out.println("combines:"+combines.size());

          return combines;

      } catch (HibernateException e) {
          e.printStackTrace();
          session.getTransaction().rollback();
          throw e;
      }
      finally
      {
    	  
    	  if (session != null) {
    		  //session.getTransaction().commit();
    	      session.close();
    	  }
      }
  }
   
  public News getNews(int idnews)
  { 
	  session = CategoryDAO.getSessionFactory().getCurrentSession();
      session.beginTransaction();
      try {
         ArrayList<News> listorder =  (ArrayList<News>)session.createQuery("from News where id="+idnews).list();
         System.out.println("listorder size"+listorder.size());
         return listorder.get(0);
      } catch (HibernateException e) {
          e.printStackTrace();
          session.getTransaction().rollback();
          throw e;
      }
      finally
      {
    	  
    	  if (session != null) {
    		  //session.getTransaction().commit();
    	      session.close();
    	  }
      }
  }
   
}
