package model;

import java.util.ArrayList;
import phanbom1.StoreUser;
public class MoCate {
    ArrayList<Category> listcates = new ArrayList<Category>();
	public ArrayList<Type> listypes = new ArrayList<Type>();
	private String page;
	ArrayList<String> css = new ArrayList<String>();
	ArrayList<String> js = new ArrayList<String>();
	public int id_type=1;
    public String title="Bán hàng điện thoại ,laptop, di động";
    public StoreUser storeuser;
    
        
	public MoCate()
	{
		css.add("reset");
		css.add("index1");
		css.add("jquery-ui");
		css.add("bootstrap");
		css.add("bootstrap-theme");
		
		js.add("jquery-1.11.1");
		js.add("jquery-ui");
		js.add("bootstrap");
	    storeuser = StoreUser.getInstance();
	}
    public StoreUser getStoreuser()
    {
        return storeuser;
    }
    public void setStoreuser(StoreUser store)
    {
        this.storeuser = store;
    }
    public void addJs(String js)
    {
    	this.js.add(js);
    }
    public void addCss(String css)
    {
    	this.css.add(css);
    }
    
    
	public ArrayList<Type> getListypes()
	{
		return listypes;
	}
	public void setListypes(ArrayList<Type> cates)
	{
		this.listypes = cates;
	}
	public ArrayList<Category> getListcates()
	{
		return listcates;
	}
	public void setListcates(ArrayList<Category> cates)
	{
		this.listcates = cates;
	}
	public void setPage(String pa)
	{
		this.page = pa;
	}
	public String getPage()
	{
		return this.page;
	}
	public void setCss(ArrayList<String> css)
	{
		this.css = css;
	}
	public ArrayList<String> getCss()
	{
		return this.css;
	}
	public void setJs(ArrayList<String> js)
	{
		this.js = js;
	}
	public ArrayList<String> getJs()
	{
		return this.js;
	}
	public void setIdType(int id_type)
	{
		this.id_type = id_type;
	}
	public int getIdType()
	{
		return this.id_type;
	}
    public void setTitle(String title)
    {
        this.title = title;
    }
    public String getTitle()
    {
        return this.title; 
    }
}
